$(window).on('load', function () {
    $('#loading').fadeOut(5, function(){
      $('#bottom_div').show();
      $('#main_div').show();
    });
  });

var emailValid = false

function activateButton(){
  console.log("masuk activateButton");
  if($('#id_email').val() != '' & $('#id_name').val() != '' & $('#id_password').val() != '' & emailValid){
    $('#button_subscribe').prop('disabled',false);
  }
}

$('#id_email').change(function validateEmail(){
  console.log("masuk validate");
  var email = $('#id_email').val();
  
  if(email != '' & email.includes('@')){
     console.log('masuk ke ajax');
    $.ajax({
      url: "/subscribe/emailValidation/",
      data: {
        'email':email
      },
      dataType: "json",
      success: function (response) {
          if(response['isTaken']){
            $('.modal-body').html('Email '+email+' has been registered, please user another email.');
            $('#ModalEmail').modal('show');
            $('#button_subscribe').prop('disabled',true);
          } else {
            emailValid = true
            activateButton();
          }  
      }
    });
  }
});

$('#id_name').change(function () { 
  activateButton();
 })

 $('#id_password').change(function () { 
  activateButton();
 })

$('#form_schedule').on('submit',function(param){
  param.preventDefault();
  var email = $('#id_email').val();
  var name = $('#id_name').val();
  var password = $('#id_password').val();
  var csrfToken = $('input[name=csrfmiddlewaretoken]').val()

  $.ajax({
    type: "POST",
    url: "/subscribe/registering/",
    data: {
      email: email,
      name : name,
      password : password,
      csrfmiddlewaretoken : csrfToken,
        },
    dataType: "json",
    success: function (response) {
      console.log('masuk success');
      $('#ModalEmailTitle').html('Congratulations');
      $('.modal-body').html('You are registered, thank you !');
      $('#ModalEmail').modal('show');
    }
  });
})