from django.shortcuts import render
from django.http import HttpResponse
from . import templates


def homepage(request):
    return render(request, 'first.html')

def resumePage(request):
    return render(request, 'second.html')

def guestPage(request):
    return render(request, 'challange.html')

# Create your views here.
