from django import forms
from .models import subModel


class subForm(forms.Form):

    email = forms.EmailField(label='', required=True,
                             widget=forms.EmailInput(attrs={'placeholder': 'Your E-Mail'}))

    name = forms.CharField(label='', required=True,
                           widget=forms.TextInput(attrs={'placeholder': 'Your name'}))

    password = forms.CharField(label='', required=True,
                               widget=forms.PasswordInput(
                                   attrs={'placeholder': "Your Password"}))

