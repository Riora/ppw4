from datetime import timezone

from django.test import TestCase, Client
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys

from django.urls import resolve
from django.utils import *
from .views import *

class Week6Test(TestCase):

    def test_6_url(self):
        response = Client().get('/subs/')
        self.assertEquals(response.status_code,200)

    def test_6asal(self):
        response = Client().get('/wkwkwkwkw')
        self.assertEquals(response.status_code, 404)

    def test_lab_6_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'subpage.html')

    def test_lab_6_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateNotUsed(response, 'assgame.html')
