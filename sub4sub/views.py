from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_protect
import json
from .forms import subForm
from .models import subModel

response = {}


# Create your views here.
def view_subscribe(request):
    return render(request, "subpage.html", {'Form': subForm})


def validate_email(request):
    dataEmail = request.GET.get('email', None)
    response['isTaken'] = subModel.objects.filter(email=dataEmail).exists()
    return JsonResponse(response)


def register_subscriber(request):
    data = request.POST
    dataEmail = data.get('email', None)
    dataName = data.get('name', None)
    dataPassword = data.get('password', None)
    model = subModel(email=dataEmail, name=dataName, password=dataPassword).save()

    response['isSuccess'] = True
    return JsonResponse(response)
