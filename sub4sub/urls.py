from django.urls import re_path,path
from django.conf.urls import url
from .views import *


#url for app
urlpatterns = [
    url(r'^emailValidation/$',validate_email),
    url(r'^registering/',register_subscriber),
    path('subs/',view_subscribe,name='subscribe-page'),
]
